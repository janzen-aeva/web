﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace riotWebApp.Models
{
    public class League
    {
        public IEnumerable<Riot> Riots { get; set; }
        public Riot Riot { get; set; }
        public IEnumerable<Champion> Champions { get; set; }
        public IEnumerable<Match> Matches { get; set; }
        public Match Match { get; set; }
    }
}
