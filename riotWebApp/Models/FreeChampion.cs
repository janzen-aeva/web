﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace riotWebApp.Models
{
    public class FreeChampion
    {
        public int maxNewPlayerLevel { get; set; }
        //[DisplayFormat(DataFormatString = "{N}")]
        public List<int> freeChampionIdsForNewPlayers { get; set; }
        //[DisplayFormat(DataFormatString = "{N}")]
        public List<int> freeChampionIds { get; set; }
    }
}
