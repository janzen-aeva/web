﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace riotWebApp.Models
{
    public class Match
    {
        public long gameId { get; set; }
        public string role { get; set; }
        public int season { get; set; }
        public string platformId { get; set; }
        public int champion { get; set; }
        public int queue { get; set; }
        public string lane { get; set; }
        public long timestamp { get; set; }
    }
    public class Root
    {
        public List<Match> matches { get; set; }
        public int startIndex { get; set; }
        public int endIndex { get; set; }
        public int totalGames { get; set; }
        public Match Match { get; set; }
    }

    public class Champion
    {
        public string id { get; set; }
        public string key { get; set; }
        public string name { get; set; }
        public string title { get; set; }
    }
    public class ChampionRoot
    {
        public Dictionary<string, Champion> data { get; set; }
        public Champion Champion { get; set; }
        //public List<Champion> champions { get; set; }
    }
}
