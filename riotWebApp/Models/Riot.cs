﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace riotWebApp.Models
{
    public class Riot
    {
        public string name { get; set; }
        public string puuid { get; set; }
        public int summonerLevel { get; set; }
        public string accountId { get; set; }
        public long revisionDate { get; set; }
        public int profileIconId { get; set; }
    }
}
