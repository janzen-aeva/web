﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace riotWebApp.Models
{
    public class Weather
    {
        public DateTime date { get; set; }
        public string City { get; set; }
        public int Temperature { get; set; }
        public char Metric { get; set; }
        public string Forecast { get; set; }
    }
}
