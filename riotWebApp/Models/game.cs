﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace riotWebApp.Models
{
    public class game
    {
        public List<Riot> Riots { get; set; }
        public IEnumerable<Champion> Champions { get; set; }
        public Riot Riot { get; set; }
        public Champion Champion { get; set; }
    }
}
