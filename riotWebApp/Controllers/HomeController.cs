﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using riotWebApp.Models;

namespace riotWebApp.Controllers
{
    public class HomeController : Controller
    {
        const string api = "RGAPI-88b44c15-f293-423a-b4f0-cf071527e164";
        public IActionResult Index(string summoner, bool? champion)
        {
            if (String.IsNullOrEmpty(summoner))
            {
                summoner = "DeliciousMilkGG";
            }

            //var api = "RGAPI-cb2758ff-d1ed-411f-af5c-af271b6bbfe3";
            WebClient client = new WebClient { }; League riots = new League { };

            var url = @"https://na1.api.riotgames.com/lol/summoner/v4/summoners/by-name/";
            url = String.Format(url + summoner + "?api_key={0}", api);

            string json = client.DownloadString(url);

            riots.Riot = Newtonsoft.Json.JsonConvert.DeserializeObject<Riot>(json);

            var games = riots.Riot.accountId;

            var gameUrl = String.Format(@"https://na1.api.riotgames.com/lol/match/v4/matchlists/by-account/{0}",games) + String.Format("?api_key={0}", api);
            string gameJson = client.DownloadString(gameUrl);
            Root root = new Root { };

            root = Newtonsoft.Json.JsonConvert.DeserializeObject<Root>(gameJson);
            return View(root);
        }

        public IActionResult ChampionSet()
        {
            FreeChampion riot = new FreeChampion { };
            //var api = "RGAPI-cb2758ff-d1ed-411f-af5c-af271b6bbfe3";
            WebClient client = new WebClient { };

            var championRotations = @"https://na1.api.riotgames.com/lol/platform/v3/champion-rotations";

            championRotations = String.Format(championRotations + "?api_key={0}", api);
                var download = client.DownloadString(championRotations);

            riot = Newtonsoft.Json.JsonConvert.DeserializeObject<FreeChampion>(download);

            return View(riot);
        }

        public IActionResult Summoner(string summoner)
        {
            if (String.IsNullOrEmpty(summoner))
            {
                summoner = "DeliciousMilkGG";
            }
            Riot riot = new Riot { };
            WebClient client = new WebClient { };

            var url = @"https://na1.api.riotgames.com/lol/summoner/v4/summoners/by-name/";
            url = String.Format(url + summoner + "?api_key={0}", api);

            var json = client.DownloadString(url);

            riot = Newtonsoft.Json.JsonConvert.DeserializeObject<Riot>(json);

            return View(riot);
        }

        public IActionResult Champions()
        {
            var url= @"http://ddragon.leagueoflegends.com/cdn/6.24.1/data/en_US/champion.json";
            WebClient client = new WebClient();

            var download = client.DownloadString(url);
            //ChampionRoot champions = new ChampionRoot { };


            var champion = Newtonsoft.Json.JsonConvert.DeserializeObject<ChampionRoot>(download).data.Values;

            return View(champion);
        }

        //public JsonResult OnGet()
        //{
            
        //    var json = Newtonsoft.Json.JsonConvert.SerializeObject(list);
        //    return Json();
        //}

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
